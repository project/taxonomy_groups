<?php

/*
 * Implements hook_menu().
 */
function taxonomy_groups_menu() {
  $vocabulary = taxonomy_vocabulary_load(variable_get('taxonomy_groups_vid', 0));
  $items['admin/content/taxonomy_groups'] = array(
    'title' => 'Taxonomy Groups',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_overview_terms', $vocabulary),
    'access arguments' => array('administer taxonomy'),
    'type' => MENU_NORMAL_ITEM,
    'file path' => drupal_get_path('module', 'taxonomy'),
    'file' => 'taxonomy.admin.inc',
  );
  $items['admin/content/taxonomy_groups/list'] = array(
    'title' => 'Groups',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/content/taxonomy_groups/add/term'] = array(
    'title' => 'Add group',
    'page callback' => 'taxonomy_add_term_page',
    'page arguments' => array($vocabulary),
    'access arguments' => array('administer taxonomy'),
    'type' => MENU_LOCAL_TASK,
    'parent' => 'admin/content/taxonomy_groups',
    'file path' => drupal_get_path('module', 'taxonomy'),
    'file' => 'taxonomy.admin.inc',
  );
  $items['admin/settings/taxonomy_groups'] = array(
    'title' => 'Taxonomy Groups',
    'description' => '',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('taxonomy_groups_configuration'),
    'access callback' => 'user_access',
    'access arguments' => array('administer taxonomy'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

function taxonomy_groups_configuration() {
  $form = array();
  $options = array();
  foreach (taxonomy_get_vocabularies() as $key => $voc) {
    if ($key != variable_get('taxonomy_groups_vid', 0)) {
      $options[$key] = $voc->name;
    }
  }
  
  $form['taxonomy_groups_autocomplete_vid'] = array(
    '#type' => 'select',
    '#title' => t('Merge terms autocomplete vocabulary'),
    '#description' => t('Select vocabulary for autocomplete lookup when merging terms'),
    '#options' => $options,
    '#default_value' => variable_get('taxonomy_groups_autocomplete_vid', 0),
  );
  
  return system_settings_form($form);
}

/*
 * Implements hook_form_FORM_ID_ater().
 */
function taxonomy_groups_form_taxonomy_overview_terms_alter(&$form, &$form_state) {
  if ($form['#vocabulary']['vid'] == variable_get('taxonomy_groups_vid', 0)) {
    foreach ($form as $key => $element) {
      if (is_array($element)) {
        if (array_key_exists('view', $element)) {
          $form[$key]['view'] = array('#value' => l($form[$key]['#term']['name'], 'admin/content/taxonomy_groups/terms_by_groups/' . $form[$key]['#term']['tid']));
        }
      }
    }
  }
}

function taxonomy_groups_form_taxonomy_form_vocabulary_alter(&$form, &$form_state) {
  if ($form['vid']['#value'] == variable_get('taxonomy_groups_vid', 0)) {
    unset($form['content_types']);
    unset($form['settings']);
    unset($form['delete']);
  }
}

function taxonomy_groups_form_taxonomy_form_term_alter(&$form, &$form_state) {
  //Overriding selectors for parents and relations - see taxonomy_form_term() and taxonomy_override_selector variable
  /*$parent = array_keys(taxonomy_get_parents($form['#term']['tid']));
  $children = taxonomy_get_tree($form['#vocabulary']['vid'], $form['#term']['tid']);
  
  // A term can't be the child of itself, nor of its children.
  foreach ($children as $child) {
    $exclude[] = $child->tid;
  }
  $exclude[] = $form['#term']['tid'];
  
  $form['advanced']['parent'] = _taxonomy_term_select(t('Parents'), 'parent', $parent, $form['#vocabulary']['vid'], t('Parent terms') .'.', 1, '<'. t('root') .'>', $exclude);
  */
  if ($form['#vocabulary']['vid'] != variable_get('taxonomy_groups_vid', 0)) {
    $form['advanced']['relations'] = _taxonomy_term_select(t('Taxonomy Group'), 'relations', array_keys(taxonomy_get_related($form['#term']['tid'])), variable_get('taxonomy_groups_vid', 0), NULL, 1, '<'. t('none') .'>', array($form['#term']['tid']));
  }
  else {
    $options = array();
    foreach(taxonomy_get_related($form['#term']['tid']) as $tid => $term) {
      $choice = new stdClass();
      $choice->option = array($term->tid => str_repeat('-', $term->depth) . $term->name);
      $options[] = $choice;
    } 
    $form['advanced']['relations'] = array(
      '#type' => 'select',
      '#title' => t('Taxonomy Group'),
      '#default_value' => array_keys(taxonomy_get_related($form['#term']['tid'])),
      '#options' => $options,
      '#description' => '',
      '#multiple' => 1,
      '#size' => min(9, count($options)),
      '#weight' => -15,
      '#theme' => 'taxonomy_term_select',
    );
  }
  $form['advanced']['relations']['#prefix'] = '<div style="display:none">';
  $form['advanced']['relations']['#suffix'] = '</div>';
  $form['#submit'][] = 'taxonomy_groups_taxonomy_form_term_submit';
}

function taxonomy_groups_taxonomy_form_term_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  if ($form_values['vid'] == variable_get('taxonomy_groups_vid', 0)) {
    db_query('DELETE FROM {term_relation} WHERE tid1 = %d OR tid2 = %d', $form_values['tid'], $form_values['tid']);
    if (!empty($form_values['relations'])) {
      foreach ($form_values['relations'] as $related_id) {
        if ($related_id != 0) {
          db_query('INSERT INTO {term_relation} (tid1, tid2) VALUES (%d, %d)', $related_id, $form_values['tid']);
        }
      }
    }
  }
}

// Remove 'edit vocabulary' link for Taxonomy Groups vocabulary
function taxonomy_groups_form_taxonomy_overview_vocabularies_alter(&$form, &$form_state) {
  unset($form[variable_get('taxonomy_groups_vid', 0)]['edit']);
}

/*
 * Implements hook_action_info().
 */
function taxonomy_groups_action_info() {
  $info['taxonomy_groups_put_into_group'] = array(
    'type' => 'term',
    'description' => t('Put/move terms into group'),
    'configurable' => TRUE,
    'hooks' => array(
      'any' => TRUE,
    ),
    'aggregate' => TRUE,
  );
  $info['taxonomy_groups_remove_from_group'] = array(
    'type' => 'term',
    'description' => t('Remove terms from group'),
    'configurable' => FALSE,
    'hooks' => array(
      'any' => TRUE,
    ),
    'aggregate' => TRUE,
  );
  $info['taxonomy_groups_merge_tags'] = array(
    'type' => 'term',
    'description' => t('Merge terms'),
    'configurable' => TRUE,
    'hooks' => array(
      'any' => TRUE,
    ),
    'aggregate' => TRUE,
  );
  
  return $info;
}

function taxonomy_groups_put_into_group_form($context) {
  /*$form['taxonomy_group'] = array(
    '#type' => 'textfield',
    '#title' => t('Taxonomy group'),
    '#description' => t('Type taxonomy group name'),
    '#default_value' => '',
    '#autocomplete_path' => 'taxonomy/autocomplete/'. variable_get('taxonomy_groups_vid', 0),
    '#maxlength' => 1024,
    '#required' => TRUE,
  );*/
  $form['taxonomy_group'] = taxonomy_form(variable_get('taxonomy_groups_vid', 0));
  $form['taxonomy_group']['#required'] = TRUE;
  return $form;
}

function taxonomy_groups_put_into_group_validate($form, $form_state) {
}

function taxonomy_groups_put_into_group_submit($form, $form_state) {
  return array('taxonomy_group' => $form_state['values']['taxonomy_group']);
}

function taxonomy_groups_put_into_group($object, $context) {
  foreach($object as $tid) {
    $related = taxonomy_get_related($tid);
    if (!empty($related)) {
      if (!array_key_exists($context['taxonomy_group'], $related)) {
        db_query('DELETE FROM {term_relation} WHERE tid1 = %d OR tid2 = %d', $tid, $tid);
        db_query('INSERT INTO {term_relation} (tid1, tid2) VALUES (%d, %d)', $tid, $context['taxonomy_group']);
      }
    }
    else {
      db_query('INSERT INTO {term_relation} (tid1, tid2) VALUES (%d, %d)', $tid, $context['taxonomy_group']);
    }
  }
}

function taxonomy_groups_remove_from_group($object, $context) {
  foreach($object as $tid) {
    db_query('DELETE FROM {term_relation} WHERE tid1 = %d OR tid2 = %d', $tid, $tid);
  }
}

function taxonomy_groups_merge_tags_form($context) {
  foreach($context['selection'] as $term) {
    $options[$term->tid] = $term->tid . ': ' . $term->term_data_name; 
  }
  $form['merge_to'] = array(
    '#type' => 'radios',
    '#title' => 'Merge terms to',
    '#options' => $options,
  );
  if (variable_get('taxonomy_groups_autocomplete_vid', 0)) {
    $form['merge_to_autocomplete'] = array(
      '#type' => 'textfield',
      '#title' => t('Autocomplete lookup'),
      //'#description' => t(''),
      '#default_value' => '',
      '#autocomplete_path' => 'taxonomy/autocomplete/' . variable_get('taxonomy_groups_autocomplete_vid', 0),
      '#maxlength' => 1024,
    );
  }
  else {
    $form['merge_to']['#required'] = TRUE;
  }
  
  return $form;
}

function taxonomy_groups_merge_tags_validate($form, $form_state) {
  $valid = FALSE;
  
  if ($form_state['values']['merge_to']) {
    $valid = TRUE;
  }
  elseif (variable_get('taxonomy_groups_autocomplete_vid', 0) && $form_state['values']['merge_to_autocomplete'] != '') {
    foreach (taxonomy_get_term_by_name($form_state['values']['merge_to_autocomplete']) as $term) {
      if ($term->vid == variable_get('taxonomy_groups_autocomplete_vid', 0)) {
        $valid = TRUE;
      }
    }
  }
  
  if (!$valid) {
    form_set_error('merge_to', 'You should select term to merge to');
  }
}

function taxonomy_groups_merge_tags_submit($form, $form_state) {
  if ($form_state['values']['merge_to']) {
    return array('merge_to' => $form_state['values']['merge_to']);
  }
  elseif (variable_get('taxonomy_groups_autocomplete_vid', 0) && $form_state['values']['merge_to_autocomplete'] != '') {
    foreach (taxonomy_get_term_by_name($form_state['values']['merge_to_autocomplete']) as $term) {
      if ($term->vid == variable_get('taxonomy_groups_autocomplete_vid', 0)) {
        return array('merge_to' => $term->tid);
      }
    }
  }
}

function taxonomy_groups_merge_tags($object, $context) {
  //change term_node entries
  $sql = 'SELECT * FROM term_node tn1 WHERE tn1.tid IN (' . implode(', ', $object) . ') ';
  $sql .= 'AND (SELECT count(*) FROM term_node tn2 WHERE tn2.nid = tn1.nid AND tn2.vid = tn1.vid AND tn2.tid = ' . $context['merge_to'] . ') = 0';
  $result = db_query($sql);
  while ($row = db_fetch_object($result)) {
    db_query('UPDATE term_node SET tid = ' . $context['merge_to'] . ' WHERE nid = ' . $row->nid . ' AND vid = ' . $row->vid . ' AND tid = ' . $row->tid);
  }
  
  //delete-insert url_alias entry for the term we are merging to - so it has the biggest pid
  $sql = 'SELECT * FROM url_alias WHERE src = "taxonomy/term/' . $context['merge_to'] . '" ORDER BY pid DESC LIMIT 1';
  $result = db_query($sql);
  if ($pid = db_fetch_object($result)) {
    db_query('DELETE FROM url_alias WHERE pid = ' . $pid->pid);
    drupal_write_record('url_alias', $pid);
  }
  
  //change url_alias entries to point to the term we are merging to and then delete the terms
  foreach ($object as $tid) {
    if ($tid != $context['merge_to']) {
      db_query('UPDATE url_alias SET src = "taxonomy/term/' . $context['merge_to'] . '" WHERE src = "taxonomy/term/' . $tid . '"');
      taxonomy_del_term($tid);
    }
  }
}

/**
 * Implements hook_views_api().
*/
function taxonomy_groups_views_api() {
  return array(
    'api' => 2.0,
  );  
}
